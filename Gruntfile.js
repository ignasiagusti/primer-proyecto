module.exports = function (grunt) { //Permitimos a node importar funcion de grunt para inyectar dependencias y config. para usarlo desde la consola
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin'
    });

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: { //Para que sass genere la versión de distribución buscando todos los archivos en la carpeta css con extensión scss, que los mande a css y les ponga la extensión .css
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },

        watch: { //Cuando hagamos un cambio del archivo .scss en la carpeta css, ejecutará automáticamente la tarea definida como css
            files: ['css/*.scss'],
            tasks: ['css']
        },

        browserSync: {
            dev: {
                bsFiles: { //Browser files
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './' //Directorio base para nuestro servidor
                    }
                }
            }
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: './',
                    src: 'images/*.{png,jpg,gif,jpeg}', //extensiones de los archivos a comprimir en la ubicación especificada
                    dest: 'dist/' //carpeta destino
                }]
            }
        },

        copy: {
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './', //current working directory
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            fonts: {
                files: [{
                    //for font-awesome
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest:'dist'                
                }]
            }
        },

        clean: {
            build: {
                src: ['dist/'] //clean the distribution folder
            }
        },

        cssmin: {
            dist: {}
        },

        uglify: {
            dist: {}
        },

        filerev: {
            options: {
                algorithm: 'md5',
                length: 15
            },
            files: {
                src: ['dist/css/*.css', 'dist/js/*.js']

            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {}
        },

        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['index.html', 'productos.html', 'ubicaciones.html', 'about.html', 'contacto.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js: ['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function (context, block) {
                                var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0,
                                    rebase: false

                                }
                            }
                        }]

                    }

                }

            }
        },

        usemin: {
            html: ['dist/index.html'],
            options: {
                assetsDir: ['dist', 'dist/css', 'dist/js']

            }

        }
    });

    grunt.registerTask('css', ['sass']); //Registramos la tarea sass para que se ejecute con grunt css
    grunt.registerTask('default', ['browserSync', 'watch']); //Tarea a ejecutar por defecto al escribir grunt en el terminal, combinamos los 3 definidos
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', [
        'clean', //Borramos el contenido de dist
        'copy', //Copiamos los archivos html a dist
        'imagemin', //Optimizamos imagenes y las copiamos a dist
        'useminPrepare', //Preparamos la configuracion de usemin
        'concat',
        'cssmin',
        'uglify',
        'filerev', //Agregamos cadena aleatoria
        'usemin' //Reemplazamos las referencias por los archivos generados por filerev
    ]);
};