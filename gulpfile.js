'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');

gulp.task('sass', async function() {  //Creamos la tarea sass
    //Nuevo stream, con un origen y donde pipe será la cadena de procesos, con dest donde lo va a volcar
    gulp.src('./css/*.scss') //Escogemos los archivos .scss de nuestra carpeta css
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css')); //Elegimos la carpeta de destino para nuestro archivo .css compilado
});

gulp.task('sass:watch', async function(){ //Habilitar watch para detectar cualquier cambio en nuestro fichero .scss
    gulp.watch('./css/*.scss', gulp.series('sass')); //Ejecutamos la tarea sass cuando cambie el archivo .scss
});

gulp.task('browser-sync', async function() { //Busca cualquier cambio que haya en los ficheros con la extensión indicada y recarga la pág 
    var files = ['./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

gulp.task('clean', function() { //elimina todo el contenido de la carpeta dist.
    return del(['dist']);
});

gulp.task('copyfonts', async function(){
    gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*') //Extracción fuentes
        .pipe(gulp.dest('./dist/fonts')); //Lo guardamos dentro de la carpeta fonts de dist
});

gulp.task('imagemin', function() { //optimiza y guarda imagenes en carpeta dist.
    return gulp.src('./img/*.{png, jpg, gif, jpeg}')
                .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
                .pipe(gulp.dest('dist/img'));
});

gulp.task('usemin', function() { //reemplazar las referencias a ficheros js y css por los minificados.
    return gulp.src('./primer-proyecto/*.html')
                .pipe(flatmap(function(stream,file) {
                    return stream
                        .pipe(usemin({
                            css: [rev()],
                            html: [function() { return htmlmin({collapseWhitespace: true}) }],
                            js: [uglify(), rev()],
                            inlinejs: [uglify()],
                            inlinecss: [cleanCss(), 'concat']
                        }));
                }))
                .pipe(gulp.dest('dist/'));

                //flat map allows us to process in paralel
});

//Tarea por defecto Browser-sync con sass-watch para escuchar cambios
gulp.task('default', gulp.series('browser-sync',gulp.parallel('sass:watch'), function (done) {
     done(); }    
));

//Tarea para hacer nueva versión de distribución
gulp.task('default', gulp.series('browser-sync',gulp.parallel('sass:watch'), function (done) {
    done(); }    
));

gulp.task('build', gulp.series('clean', gulp.parallel('copyfonts','imagemin','usemin'), function(done) {
    done(); }
));