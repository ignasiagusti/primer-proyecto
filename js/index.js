$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 3000
    });

    $("#contacto").on('show.bs.modal', function (e){ //e es el objeto que trae los datos de la ejecución del evento
      console.log('el modal contacto se está mostrando');

      $('#contactoBtn').removeClass('btn-outline-primary'); //Eliminamos la clase que define un estilo de botón
      $('#contactoBtn').addClass('btn-primary'); //Y la cambiamos por otra (para identificar que ya ha hecho click)
      $('#contactoBtn').prop('disabled', true); //deshabilita el botón, ejecutado en el show del modal (evita poder clickar muchas veces)

    });

    $("#contacto").on('shown.bs.modal', function (e){ //Cuando se terminó de mostrar
      console.log('el modal contacto ha sido mostrado');
    });

    $("#contacto").on('hide.bs.modal', function (e){ //Cuando se terminó de mostrar
      console.log('el modal contacto se está ocultando');
    }); 

    $("#contacto").on('hidden.bs.modal', function (e){ //Cuando se terminó de mostrar
      console.log('el modal contacto ha sido ocultado');

      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-outline-primary'); 
      $('#contactoBtn').prop('disabled', false); //volvemos a habilitarlo, accediendo a sus propiedades (.prop())


    }); 

  })